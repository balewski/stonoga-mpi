#include "mpi.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
/*
 This program shows how to use MPI_Gatherv.  Each processor owns myray[] and sends a
 different amount of data to the root processor allray[].  
 We use MPI_Gather first to tell the root how much data is going to be sent.
*/

#define mpi_root 0

int main(int  argc, char **argv ) {
  int rank, size, mpi_err;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
 
  mpi_err = MPI_Init( &argc, &argv );
  mpi_err = MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  mpi_err = MPI_Comm_size( MPI_COMM_WORLD, &size );
  mpi_err = MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "do scatter & gather I'm %d of %d from %s, at STAR-Barrier ...\n", 
	  rank, size,cpuName );
  fflush(stdout);

  // Synchronize before starting timing
  MPI_Barrier(MPI_COMM_WORLD);
  double totalTime=-MPI_Wtime();
  
  //............. START of the task ......
  int *displacements,*counts, *allray,totDataSize=0; // needed only for  the collecting node
  int i,numnodes=size;

  // procure input data on each node, size is different
  int myDataSize=rank+3;
  int *myray=(int*)malloc(myDataSize*sizeof(int)); assert(myray);
  for(i=0;i<myDataSize;i++) {
    myray[i]=(rank+1)*1000+i;
  }

  fprintf(stdout, "initial data size=%d on rank=%d\n",myDataSize,rank);
  //  assert(myray[0]==myDataSize); // this ise a trick - pass the dimention as the 1st element of data

  /* counts and displacement arrays are only required on the root */
  if(rank == mpi_root){
    counts=(int*)malloc(numnodes*sizeof(int));
    displacements=(int*)malloc(numnodes*sizeof(int));
  }

  //----------  step 1 ------- 
  /* we gather the counts to the root */
  mpi_err = MPI_Gather((void*)&myDataSize,1,MPI_INT, 
		       (void*)counts,  1,MPI_INT, 
		       mpi_root,MPI_COMM_WORLD);
  
  /* calculate displacements and the size of the recv array */
  if(rank == mpi_root){
    displacements[0]=0;
    for( i=1;i<numnodes;i++){
      displacements[i]=counts[i-1]+displacements[i-1];
      printf("step1 got myDataSize[%d]=%d\n",i,counts[i-1]);
    }
    totDataSize=0;
    for(i=0;i< numnodes;i++)
      totDataSize+=counts[i];
    allray=(int*)malloc(totDataSize*sizeof(int));
    printf("step1 i=%d got totDataSize=%d\n",i,totDataSize);
  }
  

  // ------  step 2 -------------------
  /* different amounts of data from each processor  is gathered to the root */
  mpi_err = MPI_Gatherv(myray, myDataSize,         MPI_INT, 
			allray,counts,displacements,MPI_INT, 
			mpi_root, MPI_COMM_WORLD);
	                
  if(rank == mpi_root){
    printf("step2 , dump of totDataSize=%d  data[]: ",totDataSize);
    for(i=0;i<totDataSize;i++)
      printf("A[%d]=%d, ",i,allray[i]);
    printf("\n");
  }

  //............. END of the task ......
  totalTime+=MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, totalTime);	       
  fflush(stdout);
    
  MPI_Finalize();
  return 0;
}
