#include "mpi.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

/*
! This program shows how to use MPI_Scatter and MPI_Gather
! Each processor gets different data from the root processor
! by way of mpi_scatter.  The data is summed and then sent back
! to the root processor using MPI_Gather.  The root processor
! then prints the global sum. 
Jan: size can be varied at compile time
*/

#define mpi_root 0
#define dataSize 40

int main(int  argc, char **argv ) {
  int rank, size, mpi_err;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
 
  mpi_err = MPI_Init( &argc, &argv );
  mpi_err = MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  mpi_err = MPI_Comm_size( MPI_COMM_WORLD, &size );
  mpi_err = MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "do scatter & gather I'm %d of %d from %s, at STAR-Barrier ...\n", 
	  rank, size,cpuName );
  fflush(stdout);

  // Synchronize before starting timing
  MPI_Barrier(MPI_COMM_WORLD);
  double totalTime=-MPI_Wtime();
  
  //............. START of the task ......
  int numnodes=size, i;
  int *myray=0,*send_ray=0,*back_ray=0;

  /* on the root create the data to be sent */
  int count=dataSize;
  myray=(int*)malloc(count*sizeof(int)); assert(myray);
  
  /* each processor will get 'count' elements from the root */
  if(rank == mpi_root){
    size=count*numnodes;
    send_ray=(int*)malloc(size*sizeof(int));  assert(send_ray);
    back_ray=(int*)malloc(numnodes*sizeof(int));  assert( back_ray );
    for(i=0;i<size;i++)
      send_ray[i]=i+1;
  }

  /*  Collective:  send or receive  different data to each processor */
  mpi_err = MPI_Scatter( send_ray, count,   MPI_INT,
			 myray,    count,   MPI_INT,
			 mpi_root, MPI_COMM_WORLD);
	                
  /* each processor does a local sum */
  int total=0;
  for(i=0;i<count;i++)
    total=total+myray[i];
  printf("myid= %d total= %d\n ",rank,total);


  /* send the local sums back to the root */
  mpi_err = MPI_Gather( &total,    1,  MPI_INT, 
			back_ray, 1,  MPI_INT, 
			mpi_root, MPI_COMM_WORLD);
  
  /* the root prints the global sum */
  if(rank == mpi_root){
    total=0;
    for(i=0;i<numnodes;i++)
      total=total+back_ray[i];
    int nval= count*numnodes;
    printf("results from all processors= %d ,  true sum=%d  nval=%d, \n ",total,nval*(nval+1)/2,nval);
  }

  //............. END of the task ......
  totalTime+=MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, totalTime);	       
  fflush(stdout);
    
  MPI_Finalize();
  return 0;
}
