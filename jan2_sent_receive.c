#include "mpi.h"
#include <stdio.h>
#include <unistd.h>  # for sleep in Ubuntu

int main(int  argc, char **argv ) {
  int rank, size;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
  double startwtime = 0.0, endwtime;  

  // for data transmission
  int  source, destRank,sourceRank,count,tag,i;
  double dataBuf[100]; 
  MPI_Status status; 
 
  
  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "send-receive demo! I'm %d of %d from %s, working ...\n", 
	  rank, size,cpuName );
  fflush(stdout);
  
  startwtime = MPI_Wtime();
  //............. START of the task ......

  destRank     = 1;
  if (rank == 0) { // sender code
    count  = 10; 
    tag    = 2001;
    for( i=0;i<10;i++) 
      dataBuf[i] = i*100+destRank; 
    int ret=MPI_Send( dataBuf, count,  MPI_DOUBLE_PRECISION, destRank,
		      tag,  MPI_COMM_WORLD);
    //      	MPI_SUCCESS
  } else if (rank ==destRank) { // receiver code
    tag   = MPI_ANY_TAG ;
    count = 10   ;
    sourceRank = MPI_ANY_SOURCE ; 
    MPI_Recv(dataBuf, count, MPI_DOUBLE_PRECISION, sourceRank, 
	     tag, MPI_COMM_WORLD, &status) ; 
      
    int st_source = status.MPI_SOURCE; 
    int st_tag    = status.MPI_TAG; 
    int st_count;
    MPI_Get_count( &status, MPI_DOUBLE_PRECISION, &st_count);
    fprintf(stdout," Receiver: rank=%d;\n  Status info: source =%d , tag=%d, count=%d  dump data:\n",rank, st_source, st_tag, st_count); 
    for( i=0;i<10;i++)  fprintf(stdout,"i%d val=%f\n",i,dataBuf[i]);
    fflush(stdout);
  }
   
  sleep(2);

  //............. END of the task ......  
  endwtime = MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, endwtime-startwtime);      fflush(stdout);
  
  MPI_Finalize();
  return 0;
}
