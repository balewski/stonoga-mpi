#include "mpi.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h> // for rnd()

/*
! This program shows how to use MPI_Alltoall.  Each processor
! send/rec a different  random number to/from other processors.  
*/

#define mpi_root 0
void seed_random(int  id);
double random_number();


int main(int  argc, char **argv ) {
  int rank, size, mpi_err;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
 
  mpi_err = MPI_Init( &argc, &argv );
  mpi_err = MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  mpi_err = MPI_Comm_size( MPI_COMM_WORLD, &size );
  mpi_err = MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "Hello world! I'm %d of %d from %s, at STAR-Barrier ...\n", 
	  rank, size,cpuName );
  fflush(stdout);

   
  //............. START of the task ......
  int numnodes=size;
  //  int *sray,*rray;
  double *scounts,*rcounts;
  int ssize,rsize,i,k,j;

  scounts=(double*)malloc(sizeof(double)*numnodes);  assert(scounts);
  rcounts=(double*)malloc(sizeof(double)*numnodes);  assert(rcounts);

  //  float z;
  /*
    ! seed the random number generator with a
    ! different number on each processor
  */
  seed_random(rank);

  /* find  data to send */
  for(i=0;i<numnodes;i++){
    scounts[i]=1000*(rank+1)+random_number();
  }

  for(i=0;i<numnodes;i++)
    printf("myid=%d SEND data[%d]=%f\n",rank,i,scounts[i]);
  fflush(stdout);
  
  // Synchronize before starting timing
  mpi_err = MPI_Barrier(MPI_COMM_WORLD);
  double totalTime=-MPI_Wtime();
  
  /* send the data */
  mpi_err = MPI_Alltoall( scounts,1,MPI_DOUBLE,
			  rcounts,1,MPI_DOUBLE,
			  MPI_COMM_WORLD);
  totalTime+=MPI_Wtime();

  for(i=0;i<numnodes;i++)
    printf("myid=%d RECV data[%d]=%f\n",rank,i,rcounts[i]);
  
  
  
  //............. END of the task ......

  printf("END for rank=%d wall clock time/sec = %f\n", rank, totalTime);	       
  fflush(stdout);
    
  mpi_err = MPI_Finalize();
  return 0;
}


//------- utils -------
void seed_random(int  id){
  srand(id+10);  // seed of 0,1 generate the same sequence
	//	printf("seed=%d, val0=%d\n",id,rand());

}

double  random_number(){
	int i;
	i=rand();
	return (float)i/RAND_MAX;
}
