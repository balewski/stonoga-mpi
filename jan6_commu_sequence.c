#include "mpi.h"
#include <stdio.h>

/*
Based on :
http://www.mcs.anl.gov/research/projects/mpi/tutorial/gropp/node115.html#Node115

NOT teste , NOT finished 

Objective: Use private communicators and attributes

Write a routine to circulate data to the next process, using a nonblocking send and receive operation. 
void Init_pipe( comm ) 
void ISend_pipe( comm, bufin, len, datatype, bufout ) 
void Wait_pipe( comm ) 
A typical use is 
Init_pipe( MPI_COMM_WORLD ) 
for (i=0; i<n; i++) { 
    ISend_pipe( comm, bufin, len, datatype, bufout ); 
    Do_Work( bufin, len ); 
    Wait_pipe( comm ); 
    t = bufin; bufin = bufout; bufout = t; 
    } 
What happens if Do_Work calls MPI routines?
127 What do you need to do to clean up Init_pipe?


*/

static int MPE_Seq_keyval = MPI_KEYVAL_INVALID;

/*@
   MPE_Seq_begin - Begins a sequential section of code.  

   Input Parameters:
.  comm - Communicator to sequentialize.  
.  ng   - Number in group.  This many processes are allowed to execute
   at the same time.  Usually one.  

@*/
void MPE_Seq_begin( comm, ng )
MPI_Comm comm;
int      ng;
{
int        lidx, np;
int        flag;
MPI_Comm   local_comm;
MPI_Status status;

/* Get the private communicator for the sequential operations */
if (MPE_Seq_keyval == MPI_KEYVAL_INVALID) {
    MPI_Keyval_create( MPI_NULL_COPY_FN, 
                       MPI_NULL_DELETE_FN, 
                       &MPE_Seq_keyval, NULL );
    }

MPI_Attr_get( comm, MPE_Seq_keyval, (void *)&local_comm, 
              &flag );
if (!flag) {
    /* This expects a communicator to be a pointer */
    MPI_Comm_dup( comm, &local_comm );
    MPI_Attr_put( comm, MPE_Seq_keyval, 
                  (void *)local_comm );
    }
MPI_Comm_rank( comm, &lidx );
MPI_Comm_size( comm, &np );
if (lidx != 0) {
    MPI_Recv( NULL, 0, MPI_INT, lidx-1, 0, local_comm, 
              &status );
    }
/* Send to the next process in the group unless we 
   are the last process in the processor set */
if ( (lidx % ng) < ng - 1 && lidx != np - 1) {
    MPI_Send( NULL, 0, MPI_INT, lidx + 1, 0, local_comm );
    }
}
//-----------------------
/*@
   MPE_Seq_end - Ends a sequential section of code.
   Input Parameters:
.  comm - Communicator to sequentialize.  
.  ng   - Number in group.  
@*/
void MPE_Seq_end( comm, ng )
MPI_Comm comm;
int      ng;
{
int        lidx, np, flag;
MPI_Status status;
MPI_Comm   local_comm;

MPI_Comm_rank( comm, &lidx );
MPI_Comm_size( comm, &np );
MPI_Attr_get( comm, MPE_Seq_keyval, (void *)&local_comm, &flag );
if (!flag) 
    MPI_Abort( comm, MPI_ERR_UNKNOWN );
/* Send to the first process in the next group OR to the first process
   in the processor set */
if ( (lidx % ng) == ng - 1 || lidx == np - 1) {
    MPI_Send( NULL, 0, MPI_INT, (lidx + 1) % np, 0, local_comm );
    }
if (lidx == 0) {
    MPI_Recv( NULL, 0, MPI_INT, np-1, 0, local_comm, &status );
    }
}

//------------------------
int main(int  argc, char **argv ) {
  int rank, size;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
  double startwtime = 0.0, endwtime;  

  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "Hello world! I'm %d of %d from %s, computing ...\n", 
	  rank, size,cpuName );
  fflush(stdout);
  startwtime = MPI_Wtime();
  
  //............. START of the task ......

  sleep(2);

  //............. END of the task ......
  endwtime = MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, endwtime-startwtime);	       
  fflush(stdout);
    
  MPI_Finalize();
  return 0;
}
