EXECS=jan1 jan2_sent_receive jan3_cpi
EXECS+=jan4_Isend-Waitsome  jan4_isfair jan4_Isend-Waitsome-Irecv jan4_unfair
EXECS+=jan5_struct jan7_bcast jan7_scatter jan7_all2all
EXECS+=jan8_gatherv jan8_all2allv


# not tested:  jan6_commu_sequence

# linux box with  MPICH installed
#MPICC=mpicc


# cori has C-wrapper for MPI
# see http://www.nersc.gov/users/computational-systems/cori/programming/compiling-codes-on-cori/
MPICC=cc

# on Ubuntu do:
MPICC=mpic++ 

all: ${EXECS}

clean:
	rm ${EXECS}

# auto-compilation of any targe on the list
%: %.c
	${MPICC} -o $@  $< 

# odl example
my_bcast: my_bcast.c
	${MPICC} -o my_bcast my_bcast.c
