#!/usr/bin/env python3
"""
Parallel numerical computation of derivative

# testing

module load python/3.7-anaconda-2019.07

salloc -N 25 -q interactive -t 2:00:00 -C haswell
srun -n 1600 python -u ./compact.py
"""

import itertools, numpy, os, re, scipy.interpolate, scipy.optimize
import  time

qubits = 2
shots = 8192
coefficients = numpy.loadtxt('colless.dat')
epsilon = 256*numpy.finfo(float).eps

#............................
def readlines(file):
    with open(file, 'rt') as f:
        g = (re.sub(r'#.*$|\s+$', '', l) for l in f)
        return iter([s for s in g if len(s)])

#............................
def probabilities(counts):
    p = (counts + 0.5)/(shots + 1)
    u = numpy.sqrt(p*(1 - p)/(shots + 1))
    return p, u

#............................
def arrays_to_vector(response, measurements):
    a = response[:, :-1]
    b = measurements[:, :-1]
    return numpy.concatenate((a, b)).reshape(-1)

#............................
def vector_to_arrays(vector):
    r = vector.reshape((-1, 3))
    a = numpy.column_stack((r, 1 - numpy.sum(r, axis=1)))
    return a[:2**qubits, :], a[2**qubits:, :]

#............................
def read_data(file):
    l = readlines(file)
    next(itertools.islice(l, 2*qubits, 2*qubits), None)
    r = numpy.loadtxt(itertools.islice(l, 2**qubits), dtype=numpy.uint)
    m = numpy.loadtxt(itertools.islice(l, None), dtype=numpy.uint)
    pr, ur = probabilities(r)
    pm, um = probabilities(m)
    return arrays_to_vector(pr, pm), arrays_to_vector(ur, um)

#............................
def bayesian(response, measurements):

    def unfold(r, e, v):
        r = r @ numpy.diag(v)
        r = numpy.diag(1/numpy.sum(r, axis=1)) @ r
        w = e @ r
        return w/sum(w)

    n = measurements.shape[0]
    v = numpy.ones(n)/n
    d = numpy.inf

    while (d > epsilon):
        w = unfold(response, measurements, v)
        d = sum(abs(w - v))/2
        v = w

    return w

#............................
def correct(response, measurements):
    m = numpy.empty(measurements.shape)
    r = response.transpose()
    for i in range(measurements.shape[0]):
        m[i, :] = bayesian(r, measurements[i, :])

    return m

#............................
def energy(index, vector):
    rm, mp = vector_to_arrays(vector)
    mc = correct(rm, mp)
    sl = mc.shape[0]//2

    xp = mc[:sl, :]
    zp = mc[sl:, :]

    ii = numpy.ones(xp.shape[0])
    xx = xp @ [1, -1, -1, 1]
    iz = zp @ [1, 1, -1, -1]
    zi = zp @ [1, -1, 1, -1]
    zz = zp @ [1, -1, -1, 1]

    aa = [(2*i/(sl - 1) - 1)*numpy.pi for i in range(sl)]
    ea = numpy.column_stack([ii, zi, xx, iz, zz]) @ coefficients[index, 1:]
    sr = scipy.interpolate.splrep(aa, ea, s=1/256)
    me = scipy.optimize.minimize_scalar(lambda x: scipy.interpolate.splev(x, sr))
    return me.fun[()]

#............................
def compute_F_errF(rank,i):

    #1) - compute gradient in all directions
    j=rank
    if j<x.shape[0] :
        F0=energy(i, x)
        x1=numpy.copy(x)
        x1[j]+=sx[j]
        F1=energy(i, x1)
        DF=(F1-F0)
        VF=DF*DF
        if verb:
            print('rank=%d FFF %f %f %.3g'%(j,F0,F1,DF))
    else:
        VF=0

    #3) -gather variance contributions
    VL = comm.gather(VF,root=0)
    VL=numpy.array(VL)

    #4)- sum var, compute error
    if rank == 0:
        #print ('master:',VL)
        totV=numpy.sum(VL)
        sigF=numpy.sqrt(totV)
        return F0,sigF,numpy.sum(VL>0)
    else:
        return 0,0,0


#=================================
#=================================
#  M A I N 
#=================================
#=================================

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
name = MPI.Get_processor_name()

verb=rank%100==0
verb=0

if verb:
    print("I am process rank %d  of %d on %s.\n" % (rank, size, name))

x, sx = read_data('experiment.out')
if rank == 0:
    startT0 = time.time()
    print('x:',x.shape,sx.shape,coefficients.shape)
    for k in range(20):
        print(k,' %.3f +/- %.3f'%(x[k],sx[k]))

N=coefficients.shape[0]
for i in range(N):
    F,errF,npc=compute_F_errF(rank,i)
    if rank == 0:
        elaTime=time.time() - startT0
        print('****i=%d  F=%.3g  +/- %.3g   num_pieces=%d, elaT=%.1f min'%(i,F,errF, npc,elaTime/60.))
    #if i>15: break

