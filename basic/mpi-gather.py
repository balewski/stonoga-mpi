#!/usr/bin/env python
"""
Parallel gather example

module load python/3.7-anaconda-2019.07
salloc -N 2 -p debug -t 00:30:00 -C knl
srun -n4 ./mpi-gather.py 
"""

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
name = MPI.Get_processor_name()

print("Hello, World! I am process rank %d of %d on %s.\n" % (rank, size, name))

if rank == 0:
   dataA = [(x+1)**x for x in range(size)]
   print ('we will be scattering dataA:',dataA)
else:
   dataA = None
   
data1 = comm.scatter(dataA, root=0)
data1 += 1
print ('rank',rank,'has data1:',data1)

newData = comm.gather(data1,root=0)

if rank == 0:
   print ('master:',newData, type(newData))
