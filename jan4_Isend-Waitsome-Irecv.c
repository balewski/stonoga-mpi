#include "mpi.h"
#include <stdio.h>
#include <unistd.h>  # for sleep in Ubuntu

int main(int  argc, char **argv ) {
  int rank, size;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
  double startwtime = 0.0, endwtime;  

  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "jan4-Isend-Waitsome-Irecv I'm %d of %d from %s, starting ...\n",  rank, size,cpuName ); fflush(stdout);
  startwtime = MPI_Wtime();
  
  //............. START of the task ......
  
  int i, index[4], count, remaining;
  int buffer[400]; // =4*100
  MPI_Request request[4];
  MPI_Status status[4];
  
  if (size != 4){
      printf("Please run with 4 processes.\n");fflush(stdout);
      MPI_Finalize();      return 1;
    }  
  if (rank == 0) { // master, only sender of messages
    for (i=0; i<size * 100; i++)	buffer[i] = i+9100;

    for (i=0; i<size-1; i++)  { // skip itself
      int tag=123;
      int destRank=i+1;
      //MPI_Isend(start,     count, datatype,   dest,  tag, comm, request) 
      MPI_Isend(&buffer[i*100], 100, MPI_INT, destRank, tag, MPI_COMM_WORLD, &request[i]);
      fprintf(stdout, "issued Isend tag=%d from rank=%d  to %d, data[0]=%d\n", i,rank,destRank,buffer[i*100]);
    }
    printf("All %d Isends started by rank=%d, do other stuff...\n",i,rank);
    fflush(stdout);    

    remaining = size-1;
    while (remaining > 0) {
      MPI_Waitsome(size-1, request, &count, index, status);
      if (count > 0)	{
	printf("%d sends completed by rank=%d\n", count,rank);fflush(stdout);
	remaining = remaining - count;
      }
    }
  } else { // rank>0 receiving
    sleep(1);

    int tag=MPI_ANY_TAG;
    int srcRank=0;
    request[0] = MPI_REQUEST_NULL;
    //MPI_Irecv(start,     count, datatype,   dest,  tag, comm, request) 
    MPI_Irecv(&buffer[0], 100, MPI_INT, srcRank, tag, MPI_COMM_WORLD, &request[0]);
 
    fprintf(stdout, "issued Irecv tag=%d on rank=%d  from %d\n do some work ...", tag,rank,srcRank); fflush(stdout);
    
    MPI_Waitsome(1, request, &count, index, status);
    if (count > 0)	{
      printf("Waitsome: %d receives completed by rank=%d from rank %d with tag %d, data[0]=%d\n", count,rank, status[0].MPI_SOURCE, status[0].MPI_TAG,buffer[0]);
      fflush(stdout);
      for (i=0;i<6;i++) printf("received  data[%d]=%d on rank=%d\n", i,buffer[i],rank);
    }
  }
    
  //............. END of the task ......
  endwtime = MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, endwtime-startwtime);	       
  fflush(stdout);
  
  MPI_Finalize();
  return 0;
}
