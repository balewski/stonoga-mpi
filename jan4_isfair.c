#include "mpi.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>  # for sleep in Ubuntu

int main(int  argc, char **argv ) {
  int rank, size;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
  double startwtime = 0.0, endwtime;  
  
  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  MPI_Get_processor_name(cpuName,&namelen);
  int nMesages=size-1;
  fprintf(stdout, "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\njan4-is-fair, I'm rank=%d of size=%d from %s, start nMesages=%d ...\n", 
	  rank, size,cpuName, nMesages );
  fflush(stdout);
  assert(size>=2); // needs at least two processe to comunicate
  startwtime = MPI_Wtime();

  //............. START of the task ......

#define large 128 
  MPI_Request requests[large]; 
  MPI_Status  statuses[large]; 
  int         indices[large]; 
  int         buf1[large], buf2[large] ; 
  int i;
  int outcount; //number of completed requests 
  memset(buf1,0,sizeof(buf1)); // sender data
  memset(buf2,0,sizeof(buf2)); // receiver data

  //... sent non-blocking messages to all *other* N-1 processes
  for (i=0; i< nMesages; i++) { 
    int destRank=(rank+i+1)%size;
    int tag=120+destRank; // tag encodes destination address
    buf1[i]=9000+destRank*100+rank; // data encodes sender & receiver
    //MPI_Isend(start, count, datatype, dest, tag, comm, request) 
    MPI_Isend( buf1+i, 1,     MPI_INT, destRank,  tag, MPI_COMM_WORLD, &requests[i] ); 
    // fprintf(stdout, "issued i=%d Isend tag=%d from rank=%d  to %d\n", i,tag,rank,destRank);   fflush(stdout);    
    //  break;
  }

  printf("All %d Isends started by rank=%d, not bother with confirmation, do other stuff...\n",i,rank);
  fflush(stdout);    
  sleep(1);//A

  //....activate receiving nonblocking messages from all N-1 processes
  for (i=0; i< nMesages; i++) { 
    int tag=MPI_ANY_TAG;
    int srcRank=(rank+i+1)%size; //  bad for N-2 messages & N=3 ???
    //? requests[i] = MPI_REQUEST_NULL;
    // store input sorted by sender address
    MPI_Irecv( buf2+srcRank, 1, MPI_INT, srcRank,  tag, MPI_COMM_WORLD, requests+i ); 
    //fprintf(stdout, "issued i=%d Irecv tag=%d  on rank=%d from %d, do some work ...\n", i,tag,rank,srcRank); fflush(stdout);
  }
  printf("All %d Irecv issued by rank=%d, do other stuff... \n",i,rank);
  fflush(stdout);    

  sleep(1);//B
  outcount=-2;
  
  while(1) { // continue untill all handles are satisfied
    MPI_Waitsome( nMesages, requests, &outcount, indices, statuses ); 
    printf("Waitsome on rank=%d outcount=%d ,ind[0]=%d, ind[1]=%d\n",rank,outcount, indices[0], indices[1]);
    if(outcount==MPI_UNDEFINED) break; // list contains no active handles

    for (i=0; i<outcount; i++) {// this may be not right, 'indices' not used 
       printf( "inspect to-rank=%d from-rank %d  tag %d, iCount=%d\n", rank,statuses[i].MPI_SOURCE, statuses[i].MPI_TAG,i );       fflush(stdout);
    } 

  }

  // for (i=0;i<size;i++) { printf("sent  buf1[%d]=%d on rank=%d\n", i,buf1[i],rank);    }    fflush(stdout);
  for (i=0;i< size;i++) { printf("received  buf2[%2d]=%d on rank=%d\n", i,buf2[i],rank);      } 
 fflush(stdout);


  //............. END of the task ......
  
    endwtime = MPI_Wtime();
    printf("END for rank=%d wall clock time/sec = %f\n", rank, endwtime-startwtime);	       fflush(stdout);
  
  MPI_Finalize();
  return 0;
}
