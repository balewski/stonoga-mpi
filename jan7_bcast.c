#include "mpi.h"
#include <stdio.h>

int main(int  argc, char **argv ) {
  int rank, size;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
 
  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "do Broadcast, I'm %d of %d from %s, at STAR-Barrier ...\n", 
	  rank, size,cpuName );
  fflush(stdout);

  // Synchronize before starting timing
  MPI_Barrier(MPI_COMM_WORLD);
  double totalTime=-MPI_Wtime();
  
  //............. START of the task ......
  int buffer[4];
  int source=0;
  int count=4;
  int i;
  if(rank == source){
    for(i=0;i<count;i++) 
      buffer[i]=400+i;
  }
  MPI_Bcast(buffer,count,MPI_INT,source,MPI_COMM_WORLD);
  fprintf(stdout, "received data on rank=%d \n",rank); 
  for(i=0;i<count;i++) 
    printf("data[%d]=%d on rank=%d \n",i,buffer[i],rank);
  fflush(stdout);  

  //............. END of the task ......
  totalTime+=MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, totalTime);	       
  fflush(stdout);
    
  MPI_Finalize();
  return 0;
}
