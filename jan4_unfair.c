#include "mpi.h"
#include <stdio.h>

int main(int  argc, char **argv ) {
  int rank, size;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
  double startwtime = 0.0, endwtime;  

  // fiarness testing
  int i, buf[1];
  int count;
  MPI_Status status;
  count=1; // to match buf size

  int nData=5; // # of data messages per process

  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "jan4-unfair! I'm %d of %d from %s, start ...\n", 
	  rank, size,cpuName );
  fflush(stdout);
  if (rank == 0) startwtime = MPI_Wtime();

  //............. START of the task ......
  
  if (rank == 0) {
    for (i=0; i<nData*(size-1); i++) {
      MPI_Recv( buf, count, MPI_INT, MPI_ANY_SOURCE, 
		MPI_ANY_TAG, MPI_COMM_WORLD, &status );
      printf( "Got Msg from rank=%d with tag %d\n", 
	      status.MPI_SOURCE, status.MPI_TAG );
    }
  }  else {
    for (i=0; i<nData; i++) {
      int tag=i;
      int destRank=0;
      MPI_Send( buf, count, MPI_INT, destRank, tag, MPI_COMM_WORLD );
      //MPI_Isend(start, count, datatype, dest, tag, comm, request) 
    }
  }
  

  //............. END of the task ......

  if (rank == 0) {
    endwtime = MPI_Wtime();
    printf("For rank=%d wall clock time/sec = %f\n", rank, endwtime-startwtime);	       
    fflush(stdout);
  }
  
  MPI_Finalize();
  return 0;
}
