#include "mpi.h"
#include <stdio.h>
#include <unistd.h>  # for sleep in Ubuntu

// Example c-struct , taken from
// http://www.mcs.anl.gov/research/projects/mpi/tutorial/gropp/node107.html#Node107

struct cell { 
  double x;
  char y;
  float z[3]; 
};

void printCell( int i, struct cell * c) {
  printf("Print content of cell[%d]: x=%.f, y=%c, z[3]=%f,%f,%f\n",i,c->x,c->y,c->z[0],c->z[1],c->z[2]);
}

struct cell cellArray[3]; // global variable -in this example
MPI_Datatype celltypeJ; // it will hold infor about my c-struct

void ini_MPI_cstruct(int rank) {
  /* set up 4 blocks */ 
  int blocklengths[4] = {1, 1, 3, 1};
  MPI_Datatype types[4] = {MPI_DOUBLE, MPI_CHAR,MPI_FLOAT, MPI_UB};
  MPI_Aint displacements[4];
  MPI_Aint base;

  /* initialize types and displs with addresses of items */ 
  MPI_Address(&cellArray[0].x, &displacements[0]);
  MPI_Address(&cellArray[0].y, &displacements[1]);
  MPI_Address(&cellArray[0].z, &displacements[2]);
  MPI_Address(&cellArray[1].x, &displacements[3]); // this is for UB?
  base = displacements[0];
  int i;
  for (i = 0; i < 4; ++i) displacements[i] -= base;
  MPI_Type_struct(4, blocklengths, displacements,types, &celltypeJ);
  MPI_Type_commit(&celltypeJ);
  printf( "cell-struct initialized on rank %d\n",rank); fflush(stdout);
}
//------------------------------

int main(int  argc, char **argv ) {
  int rank, size;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
  double startwtime = 0.0, endwtime;  

  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "send-receive C-struct I'm %d of %d from %s, computing ...\n", 
	  rank, size,cpuName );
  fflush(stdout);
  startwtime = MPI_Wtime();
  
  //............. START of the task ......
  ini_MPI_cstruct(rank);
  int    sourceRank=0, destRank = 1;
  int   count  = 3; 
  int tag    = 2001;
  int i;
  
  if (rank == 0) { // sender code
    for( i=0;i<count;i++) {// fill c-struct with data
      cellArray[i].x=500+i;
      cellArray[i].y='A'+i;
      cellArray[i].z[0]=100+i;
      cellArray[i].z[1]=200+i;
      cellArray[i].z[2]=300+i;
    }    
    MPI_Send( cellArray, count,  celltypeJ, destRank, tag,  MPI_COMM_WORLD);    
  } else if (rank ==destRank) { // receiver code
    tag   = MPI_ANY_TAG ;
    sourceRank = MPI_ANY_SOURCE ; 
    MPI_Status status; 
    int st_source, st_tag, st_count;
    MPI_Recv(cellArray, count,  celltypeJ, sourceRank, tag, MPI_COMM_WORLD, &status) ; 
      
    st_source = status.MPI_SOURCE; 
    st_tag    = status.MPI_TAG; 
    MPI_Get_count( &status, celltypeJ , &st_count);
    fprintf(stdout," Receiver: rank=%d;\n  Status info: source =%d , tag=%d, count=%d  dump data:\n",rank, st_source, st_tag, st_count); 
    for( i=0;i<count;i++)  printCell(i,cellArray+i);
    fflush(stdout);
  }

  sleep(2);

  //............. END of the task ......
  endwtime = MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, endwtime-startwtime);	       
  fflush(stdout);
    
  MPI_Finalize();
  return 0;
}
