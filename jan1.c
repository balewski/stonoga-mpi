#include "mpi.h"
#include <stdio.h>
#include <unistd.h>  # for sleep in Ubuntu

int main(int  argc, char **argv ) {
  int rank, size, mpi_err;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
 
  mpi_err = MPI_Init( &argc, &argv );
  mpi_err = MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  mpi_err = MPI_Comm_size( MPI_COMM_WORLD, &size );
  mpi_err = MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "Hello world! I'm %d of %d from %s, at START-Barrier ...\n", 
	  rank, size,cpuName );
  fflush(stdout);

  // Synchronize before starting timing
  mpi_err = MPI_Barrier(MPI_COMM_WORLD);
  double totalTime=-MPI_Wtime();
  
  //............. START of the task ......

  sleep(2);

  //............. END of the task ......
  totalTime+=MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, totalTime);	       
  fflush(stdout);
    
  mpi_err = MPI_Finalize();
  return 0;
}
