#include "mpi.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h> // for memset

/*
! This program shows how to use MPI_Alltoallv.  Each processor
! send/rec a different and random amount of data to/from other
! processors.  
! We use MPI_Alltoall to tell how much data is going to be sent.
*/
#define mpi_root 0
void seed_random(int  id);
double random_number();

int main(int  argc, char **argv ) {
  int rank, size, mpi_err;
  int    namelen;
  char   cpuName[MPI_MAX_PROCESSOR_NAME];
 
  mpi_err = MPI_Init( &argc, &argv );
  mpi_err = MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  mpi_err = MPI_Comm_size( MPI_COMM_WORLD, &size );
  mpi_err = MPI_Get_processor_name(cpuName,&namelen);

  fprintf(stdout, "do scatter & gather: I'm rank%d of size=%d cpuName=%s, at STAR-Barrier ...\n", 
	  rank, size,cpuName );
  fflush(stdout);

  // Synchronize before starting timing
  MPI_Barrier(MPI_COMM_WORLD);
  double totalTime=-MPI_Wtime();
  
  //............. START of the task ......
  int numnodes=size;
  int *sray,*rray; // has data
  int *sdisp,*scounts,*rdisp,*rcounts; // has dimensions
  int i,k,j; // working variables

  scounts=(int*)malloc(sizeof(int)*numnodes);
  rcounts=(int*)malloc(sizeof(int)*numnodes);
  sdisp=(int*)malloc(sizeof(int)*numnodes);
  rdisp=(int*)malloc(sizeof(int)*numnodes);
  /*
    ! seed the random number generator with a
    ! different number on each processor
  */
  seed_random(rank);

  /* find out how much data to send */
  int totData=0;
  for(i=0;i<numnodes;i++){
    scounts[i]=(int)(5.*random_number())+1;
    totData+=scounts[i];
  }
 
  for(i=0;i<numnodes;i++)
    printf("  myid=%d  dataSize[%d]=%d  of tot=%d*(int) \n",rank,i,scounts[i],totData);
  memset(rcounts,0,sizeof(rcounts));


  //..... step A ......
  /* tell the other processors how much data is coming */
  mpi_err = MPI_Alltoall( scounts,1,MPI_INT,
			  rcounts,1,MPI_INT,
			  MPI_COMM_WORLD);

  for(i=0;i<numnodes;i++)
    printf("  myid=%d   Rcounts[%d]=%d , end of step-A\n",rank,i,rcounts[i]);


  //........ step B .....
  /* calculate displacements and the size of the arrays */
  sdisp[0]=0;
  for(i=1;i<numnodes;i++){
    sdisp[i]=scounts[i-1]+sdisp[i-1];
  }
  rdisp[0]=0;
  for(i=1;i<numnodes;i++){
    rdisp[i]=rcounts[i-1]+rdisp[i-1];
  }
  int ssize=0, rsize=0;
  for(i=0;i<numnodes;i++){
    ssize=ssize+scounts[i];
    rsize=rsize+rcounts[i];
  }


  //....... step C ......
  /* allocate send- and rec-arrays, fill send-array with data */
  sray=(int*)malloc(sizeof(int)*ssize);  assert(sray);
  rray=(int*)malloc(sizeof(int)*rsize);  assert(rray);
  for(i=0;i<ssize;i++)
    sray[i]=(int)1000*rank+20*random_number();

  for(i=0;i<ssize;i++)
    printf("  myid=%d Sray[%d]=%d  step-C\n",rank,i,sray[i]);
  memset(rray,0,sizeof(rray));

  //....... step D .....
  /* send/rec different amounts of data to/from each processor */
  mpi_err = MPI_Alltoallv(sray,scounts,sdisp,MPI_INT,
			  rray,rcounts,rdisp,MPI_INT,
			  MPI_COMM_WORLD);

  for(i=0;i<rsize;i++)
    printf("  myid=%d   Rray[%d]=%d  step-D\n",rank,i,rray[i]);
	                
  //............. END of the task ......
  totalTime+=MPI_Wtime();
  printf("END for rank=%d wall clock time/sec = %f\n", rank, totalTime);	       
  fflush(stdout);
    
  MPI_Finalize();
  return 0;
}


//------- utils -------
void seed_random(int  id){
  srand(id+10);  // seed of 0,1 generate the same sequence
	//	printf("seed=%d, val0=%d\n",id,rand());

}

double  random_number(){
	int i;
	i=rand();
	return (float)i/RAND_MAX;
}
